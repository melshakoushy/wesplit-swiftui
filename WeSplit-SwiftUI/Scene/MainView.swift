//
//  ContentView.swift
//  WeSplit-SwiftUI
//
//  Created by Shakoushy on 19/01/2021.
//

import SwiftUI

struct MainView: View {
    
    // MARK:- Variables
    @State private var amount = ""
    @State private var numberOfPeople = 2
    @State private var tipPercantage = 2
    
    let percantage = [10, 15, 20, 25, 0]
    
    var totalPerPerson: Double {
        let peopleCount = Double(numberOfPeople + 2)
        let tipSelection = Double(percantage[tipPercantage])
        let orderAmount = Double(amount) ?? 0
        
        let tipValue = orderAmount / 100 * tipSelection
        let grantTotal = orderAmount + tipValue
        let amountPerPerson = grantTotal / peopleCount
        return amountPerPerson
    }
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Amount", text: $amount).keyboardType(.decimalPad)
                    
                    Picker("Number of people", selection: $numberOfPeople) {
                        ForEach(2..<100) {
                            Text("\($0) people")
                        }
                    }
                }
                
                Section(header: Text("Select the percantage of tip")) {
                    Picker("Tip percentage", selection: $tipPercantage) {
                        ForEach(0 ..< percantage.count) {
                            Text("\(self.percantage[$0])%")
                        }
                    }.pickerStyle(SegmentedPickerStyle())
                }
                
                Section {
                    Text("$\(totalPerPerson , specifier: "%.2f")")
                }
            }.navigationTitle("WeSplit")
        }
    }
    
    struct MainView_Previews: PreviewProvider {
        static var previews: some View {
            MainView()
        }
    }
}
