//
//  WeSplit_SwiftUIApp.swift
//  WeSplit-SwiftUI
//
//  Created by Shakoushy on 19/01/2021.
//

import SwiftUI

@main
struct WeSplit_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
